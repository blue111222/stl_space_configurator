#pragma once
#include <new>

typedef void (*POOM)();

template<int inst>
class MallocAllocTemplate
{
public:
	static void* Allocate(size_t size)
	{
		void* res = malloc(size);
		if(0 == res)
			res = OOM_Malloc(size);

		return res;
	}

	static void DeAllocate(void* p, size_t /*size*/)
	{
		free(p);
	}

	static POOM SetMallocHandle(POOM pOOM)
	{
		POOM old = _pOOM;
		_pOOM = pOOM;
		return old;
	}
private:
	static void* OOM_Malloc(size_t size)
	{
		void* res = NULL;
		for(;;)
		{
			if(NULL == _pOOM)
				throw bad_alloc();

			_pOOM();
			res = malloc(size);
			if(res)
				return res;
		}
	}
private:
	static POOM _pOOM;
};

template<int inst>
POOM MallocAllocTemplate<inst>::_pOOM = 0;

typedef MallocAllocTemplate<0> MallocAlloc;


// 内存池
template<int inst>
class DefaultAllocTemplate
{
	enum {__ALIGN = 8};
	enum {__MAX_BYTES = 128};
	enum {__NFREELISTS = __MAX_BYTES/__ALIGN};

public:
	static void* Allocate(size_t size)
	{
		// 字节数超过128
		if(size > __MAX_BYTES)
		{
			return MallocAlloc::Allocate(size);
		}

		size_t index = FREELIST_INDEX(size);
		if(NULL == _freeList[index])
		{
			return ReFill(ROUND_UP(size))
		}

		// 将该自由链表中第一个内存块交给用户使用
		void* res = (void*)_freeList[index];

		_freeList[index] = _freeList[index]->_freeListLink;
		return res;
	}

	static void DeAllocate(void* p, size_t size)
	{
		if(size > __MAX_BYTES)
		{
			MallocAlloc::DeAllocate(p, size);
			return;
		}

		size_t index = FREELIST_INDEX(size);
		((Obj*)p)->_freeListLink = _freeList[index];
		_freeList[index] = (Obj*)p;
	}

private:
	static void* ChunkAlloc(size_t& nobjs, size_t size)
	{
		// 内存池中剩余字节数
		size_t leftBytes = _endFree - _startFree;

		// 计算总共需要的自己数
		size_t totalBytes = nobjs * size;

		char* res;
		if(leftBytes >= totalBytes)
		{
			res = _startFree;
			_startFree += totalBytes;
			return res;
		}
		else if(leftBytes >= size)
		{
			nobjs = leftBytes / size;
			totalBytes = nobjs * size;
			res = _startFree;
			_startFree += totalBytes;
			return res;
		}
		else
		{
			// 0. 将内存池中剩余的空间清理干净
			size_t index = 0;
			if(leftBytes > 0)
			{
				index = FREELIST_INDEX(leftBytes);
				((Obj*)_startFree)->_freeListLink = _freeList[index];
				_freeList[index] = (Obj*)_startFree;
			}

			// 1. 系统--->充足
			size_t Byte2Get = 2 * totalBytes + ROUND_UP(_heapSize>>4);
			_startFree = (char*)malloc(Byte2Get);
			if(NULL ==_startFree)
			{
				// 2. 在自由链表中找大于size的内存块
				index = FREELIST_INDEX(size);
				while(index < __NFREELISTS)
				{
					if(_freeList[index])
					{
						_startFree = _freeList[index];
						_endFree = _startFree + (index+1)*__ALIGN;
						_freeList[index] = _freeList[index]->_freeListLink;
						return ChunkAlloc(nobjs, size);
					}

					++index;
				}

				// 3. 向一级空间配置器要空间
				_endFree = 0;
				_startFree = MallocAlloc::Allocate(Byte2Get);
			}

			_endFree = _startFree + Byte2Get;
			_heapSize += Byte2Get;
			return ChunkAlloc(nobjs, size);
		}
	}

	static void* ReFill(size_t size)
	{
		size_t nobjs = 20;
		char* chunk = ChunkAlloc(nobjs, size);

		if(1 == nobjs)
			return chunk;

		char* res = chunk;
		chunk = chunk + size;
		size_t index = FREELIST_INDEX(size);
		size_t idx = 1;
		while(idx < nobjs)
		{
			Obj* cur = (Obj*)chunk;
			cur->_freeListLink = _freeList[index];
			_freeList[index] = cur;
			chunk += size;
		}

		return res;
	}

	static  size_t FREELIST_INDEX(size_t bytes)
	{
		return (((bytes) + __ALIGN-1)/__ALIGN - 1);
	}

	static size_t ROUND_UP(size_t bytes)
	{
		return (((bytes) + __ALIGN-1) & ~(__ALIGN - 1));
	}
private:
	static char* _startFree;
	static char* _endFree;
	static size_t _heapSize;

	// 结点
	union Obj
	{
		Obj* _freeListLink;
		char clientData[1];
	};

	static Obj* _freeList[__NFREELISTS];
};


template<int inst>
char* DefaultAllocTemplate<inst>::_startFree = 0;

template<int inst>
char* DefaultAllocTemplate<inst>::_endFree = 0;

template<int inst>
size_t DefaultAllocTemplate<inst>::_heapSize = 0;

template<int inst>
Obj* DefaultAllocTemplate<inst>::_freeList[__NFREELISTS] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};


typedef DefaultAllocTemplate<0> DefaultAlloc;

#ifdef USE_MALLOC
  typedef MallocAlloc _Alloc_;
#else
  typedef DefaultAlloc _Alloc_;
#endif


void TestAllocate()
{
	char* p1 = (char*)_Alloc_::Allocate(200);
	_Alloc_::DeAllocate(p1, 200);
}